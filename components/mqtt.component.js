var logger = require('./logger.component.js')(module);
var mqtt = require('mqtt');
var config = require('config');
var localizer = require('./localizer.component.js');

var options = {};
if( config.get('atlas.broker.authentication') == "enable" ){
  options.username = config.get('atlas.broker.username');
  options.password = config.get('atlas.broker.password');
}

mqclient = mqtt.connect(config.get('atlas.broker.host'), options);

mqclient.on('connect', function () {
  logger.info("Connection established on " + config.get('atlas.broker.host'));
  localizer.init(mqclient);
  logger.info("Going to subscribe to topics");
  config.get('clients.mqtt.subscriptions').forEach(function(value){
    logger.info("Subscribe to topic: " + value);
    mqclient.subscribe(value)
  });
  logger.info("Subscription to topics finished.");

});

mqclient.on('reconnect', function () {
  localizer.init(null);
  logger.info("Attempting reconnection on " + config.get('atlas.broker.host'));
});
mqclient.on('close', function () {});
mqclient.on('offline', function () {});

mqclient.on('error', function (error) {
  logger.error("Unable to initialize connection on " + config.get('atlas.broker.host') + ", " + error);
});

mqclient.on('message', function (topic, message) {
  logger.info("Message received to topic: " + topic);
  localizer.forward(topic, message);
});
