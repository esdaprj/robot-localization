const path = require('path');
var winston = require('winston');
var getLabel = function (modulename) {
    var parts = modulename.filename.split(path.sep);
    return parts[parts.length - 2] + path.sep + parts.pop();
};

module.exports = function (modulename) {
    return new winston.Logger({
        transports: [
            new winston.transports.Console({
                label: getLabel(modulename),
                json: false,
                timestamp: true,
                depth:true,
                colorize:'all'
            })
        ]
    });
};
