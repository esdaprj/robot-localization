var logger = require('./logger.component.js')(module);
var config = require('config');
var protobuf = require("protobufjs");
var math = require('mathjs');
var _ = require('underscore')._;
const EventEmitter = require('events');
const emitter = new EventEmitter();

var maps = [];
var publisher = null;

function getDatatype(topic){
  let type = topic.split("/");
  return type[config.get("emit_index")];
}

function getHouseFromTopic(topic){
  let type = topic.split("/");
  return type[type.length-1];
}

emitter.on('maps', function (topic, data) {
  if( publisher == null ){
    logger.warn("Connector not initialized...");
    return;
  }
  try{
    let aalhouse = getHouseFromTopic(topic);
    if( aalhouse === undefined ) {
      logger.error("Invalid AALHouse")
      return;
    }
    logger.info("Going to load map for AALHouse["+aalhouse+"]");
    publisher.publish(config.get("api.mqtt.maps").replace("{aalhouse}",aalhouse),"");
  }catch(e){
    logger.error("Unable to parse JSON",e);
  }
});

protobuf.load(config.get('protobuf.file'))
.then(function(root) {
  logger.info("Loading protocol buffer messages")
  AALHouseFingerPrints = root.lookupType("gr.esda.atlas.messages.AALHouseFingerPrints");
  AALEvent = root.lookupType("gr.esda.atlas.messages.AALEvent");

  emitter.on('fingerprints', function (topic, data) {
    let fgrPrs = AALHouseFingerPrints.decode(data);
    maps['aalhouse_'+fgrPrs.aalhouse] = fgrPrs.fingerprints;
    logger.info("Receive AALHouse["+fgrPrs.aalhouse+"] fingerprints, Total fingerprints fetched: " + fgrPrs.fingerprints.length);
  });//END fingerprints

  emitter.on('coordinates', function (topic, data) {

    let aalhouseId = "aalhouse_"+getHouseFromTopic(topic);
    if( !( aalhouseId in maps) ){
        logger.warn("No for "+aalhouseId+" loaded");
        return;
    }
    try{
      let aalEvt = AALEvent.decode(data);
      emitter.emit("match_coordinates",aalhouseId, aalEvt);
    }catch(e){
      logger.error("Unable to parse JSON",e);
    }
  });//END coordinates

  emitter.on('match_coordinates', function (aalhouse, aalEvt) {
    let coordinates = aalEvt.coordinate;
    logger.info("Going to match physical["+coordinates.x+","+coordinates.y+","+coordinates.z+"] to virtual coordinates, for AALHouse["+aalhouse+"]");
    let distance = 100000000;
    let tmp = 0;
    let virtual = {x: 0, y: 0, z: 0};
    _.each(maps[aalhouse], function(cord){
      tmp = math.abs(math.sqrt(math.pow((coordinates.x-cord.objectX),2) + math.pow((coordinates.y-cord.objectY),2)));
      if( tmp < distance ) {
        distance = tmp;
        virtual.x = cord.virtualX;
        virtual.y = cord.virtualY;
      }
    });
    logger.info("Best distance calculated["+distance+"], Physical Coordinates["+coordinates.x+","+coordinates.y+"], Virtual Coordinates Matched ["+virtual.x+","+virtual.y+"]");
    aalEvt.coordinate.x = virtual.x;
    aalEvt.coordinate.y = virtual.y;
    aalEvt.coordinate.z = virtual.z;
    let buffer = AALEvent.encode(aalEvt).finish();
    publisher.publish(config.get("api.mqtt.websocket"),buffer);
  });//Finish match_coordinates

});

module.exports = {
  init: function(client){
    publisher = client;
  },
  forward: function(topic, msg){
    emitter.emit(getDatatype(topic), topic, msg);
  }
};
